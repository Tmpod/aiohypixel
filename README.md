# aiohypixel 
A modern asynchronous Hypixel API wrapper with full API coverage written in Python.

## Installation

It's very simple! Just use `pip`, like this: `pip install aiohypixel`.  
If you want to install the staging build, you can do so by running `pip install --index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple aiohypixel` instead.

## How do you use this?

If you like to read, you can check the documentation [**here**](https://tmpod.gitlab.io/aiohypixel)!

If not, here's a crash course:

> **TODO**

~~You can also check the `examples` folder for some more handsdown examples on how to use this library.~~ **TODO**

## Ran into any issues?

No problem! Just head out to the [**Issues**](https://gitlab.com/Tmpod/aiohypixel/issues) and see if there's any opened or closed issue regarding situation. If not, feel free to open one!  
You can also join the support [**Matrix**](https://matrix.org) room at **`#aiohypixel:matrix.org`**.

> ⚠ This module is still under heavy development, and so there may occur major changes!

Keep an eye on this page, as it will be updated when major progress is done :wink:

